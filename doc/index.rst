CNVkit: Copy number from targeted DNA sequencing
================================================

:Author: Eric Talevich
:Contact: eric.talevich@ucsf.edu
:Source code: http://github.com/etal/cnvkit
:License: BSD

CNVkit is a Python library and command-line software toolkit to infer and
visualize copy number from targeted DNA sequencing data. It is designed for use
with hybrid capture, including both whole-exome and custom target panels, and
short-read sequencing platforms such as Illumina.

.. toctree::

    quickstart

Command line usage
------------------

.. toctree::
    :maxdepth: 3

    pipeline
    plots
    reports
    importexport
    scripts


Python API
----------

.. toctree::
   :maxdepth: 4

   cnvlib


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

